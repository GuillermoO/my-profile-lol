import React, { useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { useFormik } from 'formik';
import Grid from '@material-ui/core/Grid';
import axios from 'axios';


const useStyles = makeStyles({
  container: {
    margin:20,
    padding: 40,
    backgroundColor: '#EDF3EE',
    flexGrow: 1, 
  },
});

export default function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

function Home({ props }) {

  const classes = useStyles(props);

  const [invocador, setInvocador] = useState({});

  const formik = useFormik({
    initialValues: {
      invocador: '',
    },
    onSubmit: async (values) => {
      axios.get(`https://la2.api.riotgames.com/lol/summoner/v4/summoners/by-name/${values.invocador}?api_key=RGAPI-35a7e9d0-67ce-4b5e-b6af-dcf575fdacd5`)
      .then(results => setInvocador(results.data))
    },
  });

  return (<div>
    <Box className={classes.container}>      
      <form onSubmit={formik.handleSubmit}>
      <Grid container alignItems="center" alignContent="center">
        <Grid item xs={3}></Grid>
        <Grid item xs={3} style={{ marginBottom:15, marginRight:20}}>            
            <TextField
              fullWidth              
              id="invocador"
              name="invocador"
              label="Invocador"
              value={formik.values.invocador}
              onChange={formik.handleChange}
              error={formik.touched.invocador && Boolean(formik.errors.invocador)}
              helperText={formik.touched.invocador && formik.errors.invocador}
            />
        </Grid>
        <Grid container item xs={3}>
          <Button color="primary" variant="contained" fullWidth type="submit">
              BUSCAR
          </Button>
        </Grid>
        </Grid> 
        </form>
        { invocador.id ? <Grid container alignItems="center" alignContent="center">
          <Grid item xs={5}></Grid>
        <Grid item xs={3} style={{ marginTop: 20 }}>
            <h4>Name : { invocador.name }</h4>
            <h4>Level: { invocador.summonerLevel }</h4>
            <Button color="secondary" variant="contained" name="">
              Campeones Jugados
            </Button>
        </Grid>        
        </Grid>  : ''}
    </Box>
  </div>);
}